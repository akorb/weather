global class WeatherRunner implements Schedulable{
    
    global void execute(SchedulableContext ctx) {
        WeatherUpdateBatch batch =   new WeatherUpdateBatch();
        Database.executeBatch(batch,50);
    }
}
