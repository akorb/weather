import { LightningElement, api, wire , track} from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
const NAME_FIELD = 'Account.Location__c';
const NAME_LON_FIELD = 'Account.Location_Lon__c';
const NAME_LAT_FIELD = 'Account.Location_Lat__c';
const NAME_TEMP_FIELD = 'Account.Temperature__c';
const NAME_WIND_SPEED_FIELD = 'Account.Wind__c';
const NAME_DESCRIPTION_FIELD = 'Account.Weather_Description__c';
const NAME_ICON_FIELD = 'Account.Icon__c';
import getCurrentWeather from '@salesforce/apex/WeatherSevice.getCurrentWeather';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';


const fields = [NAME_FIELD,NAME_LON_FIELD,NAME_LAT_FIELD,NAME_WIND_SPEED_FIELD,NAME_TEMP_FIELD,NAME_DESCRIPTION_FIELD,NAME_ICON_FIELD];

export default class Weather extends LightningElement {
    
    @api recordId;

    @track mapMarkers = [];
    zoomLevel = 10;
   
    temperature;
    windSpeed;
    description;
    lat;
    lon;
    code;
    link;

    name;
    @wire(getRecord, { recordId: '$recordId', fields: fields })
    loadCityName({ error, data }) {
      if (error) {
          console.log(error)
      } else if (data) {
        this.name =  getFieldValue(data, NAME_FIELD);
        this.temperature = getFieldValue(data, NAME_TEMP_FIELD);
        this.windSpeed = getFieldValue(data,NAME_WIND_SPEED_FIELD);
        this.description = getFieldValue(data,NAME_DESCRIPTION_FIELD);
        this.lat = getFieldValue(data,NAME_LAT_FIELD);
        this.lon = getFieldValue(data,NAME_LON_FIELD);
        this.code = getFieldValue(data,NAME_ICON_FIELD);
        this.link = "http://openweathermap.org/img/wn/" + this.code + "@2x.png";
        
        this.mapMarkers = [{
            location: {
                Latitude: this.lat,
                Longitude: this.lon
            },
            title: this.name
        }]
        
      }
    }
}