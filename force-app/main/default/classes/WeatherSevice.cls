global class WeatherSevice {
 
    @AuraEnabled 
   global static Map<String,Weather> getCurrentWeather(List<String> locations){
    Map<String, Weather> weathers = new Map<String,Weather>();
       for (String location : locations) {
           if (location != null) {
               
             final String apiLink =   'http://api.openweathermap.org/data/2.5/weather?q='; 
             final String accessToken = '&appid=6ef4d7f4e0ca44cdd6f7a66af63031ca';
             final String metricUnits = '&units=metric';

        HttpRequest req = new HttpRequest();
        req.setEndpoint( apiLink + location + + accessToken + metricUnits);
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        JSONParser parser = JSON.createParser(res.getBody());
        Weather newWeather;
            if (res.getStatusCode() == 200) {
    

            newWeather = new Weather();
    
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                Map<String, Object> weather = (Map<String, Object>) ((List<Object>)results.get('weather')).get(0);
                Map<String, Object> main = (Map<String, Object>) (results.get('main'));
                Map<String, Object> wind = (Map<String, Object>) (results.get('wind'));
                String locationJson = (String) (results.get('name'));
                Map<String, Object> cord = (Map<String, Object>) (results.get('coord'));

                            newWeather.cityTemp = (Decimal) main.get('temp');
                            newWeather.cityName = locationJson;
                            newWeather.icon =(String) weather.get('icon');
                            newWeather.cityWindSpeed = (Decimal)wind.get('speed');
                            newWeather.cityLat = (Decimal)cord.get('lat');
                            newWeather.cityLong = (Decimal) cord.get('lon');
                            newWeather.description = (String) weather.get('description');
        
    
        }

        weathers.put(newWeather.cityName, newWeather);

   }
   
}
return weathers; 
}
 

@future(callout=true)
public static void  getWeather(String location, String id){
    
    Account account =  [SELECT ID, Location__c,Temperature__c,Weather_Description__c,Wind__c,Location_Lat__c,Location_Lon__c FROM Account where ID = :id];
          final String apiLink =   'http://api.openweathermap.org/data/2.5/weather?q='; 
          final String accessToken = '&appid=6ef4d7f4e0ca44cdd6f7a66af63031ca';
         final String metricUnits = '&units=metric';

        HttpRequest req = new HttpRequest();
        req.setEndpoint( apiLink + location + + accessToken + metricUnits);
    req.setMethod('GET');
    Http http = new Http();
    HTTPResponse res = http.send(req);
    JSONParser parser = JSON.createParser(res.getBody());
    Weather newWeather;
if (res.getStatusCode() == 200) {
    

    newWeather = new Weather();
    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
    Map<String, Object> weather = (Map<String, Object>) ((List<Object>)results.get('weather')).get(0);
    Map<String, Object> main = (Map<String, Object>) (results.get('main'));
    Map<String, Object> wind = (Map<String, Object>) (results.get('wind'));
    String locationJson = (String) (results.get('name'));
    Map<String, Object> cord = (Map<String, Object>) (results.get('coord'));

               newWeather.cityTemp = (Decimal) main.get('temp');
               newWeather.cityName = locationJson;
               newWeather.icon =(String) weather.get('icon');
               newWeather.cityWindSpeed = (Decimal)wind.get('speed');
               newWeather.cityLat = (Decimal)cord.get('lat');
               newWeather.cityLong = (Decimal) cord.get('lon');
               newWeather.description = (String) weather.get('description');
    }
            account.Temperature__c = newWeather.cityTemp;
            account.Weather_Description__c = newWeather.description;
            account.Wind__c = newWeather.cityWindSpeed;
            account.Location_Lat__c = newWeather.cityLat;
            account.Location_Lon__c = newWeather.cityLong;
            account.Icon__c = newWeather.icon;
           
        update account;
    }

}