global class WeatherUpdateBatch implements 
Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts{

    Set<String> locationWithWeather = new Set<String>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
       
        return Database.getQueryLocator('SELECT ID, Location__c,Temperature__c,Weather_Description__c,Wind__c,Location_Lat__c,Location_Lon__c FROM Account');
    }
        

    global void execute(Database.BatchableContext bc, List<Account> scope){
        Map<String,Weather> weathers = new Map<String,Weather>();

        // process each batch of records
        //check if i have already data about wether for the same location
       
        List<String> locationToCheckWeather = new List<String>();
        List<Account> accounts = new List<Account>();

        for (Account account : scope) {
            if (locationWithWeather.add(account.Location__c)) {
                locationToCheckWeather.add(account.Location__c);
            }
            
        }


        //get data about current weather
        
            Map<String,Weather> newWeathers = WeatherSevice.getCurrentWeather(locationToCheckWeather);
            weathers.putAll(newWeathers);
        
       
//update fields from accounts with current weather
 for (Account account : scope) {
        if (account.Location__c != null && account != null) {
            if (weathers.get(account.Location__c) != null) {
                account.Temperature__c = weathers.get(account.Location__c).cityTemp;
            account.Weather_Description__c = weathers.get(account.Location__c).description;
            account.Wind__c = weathers.get(account.Location__c).cityWindSpeed;
            account.Location_Lat__c = weathers.get(account.Location__c).cityLat;
            account.Location_Lon__c = weathers.get(account.Location__c).cityLong;
            account.Icon__c = weathers.get(account.Location__c).icon;
            accounts.add(account);
            }
            
    }
    
    update accounts;
        }
       
    }    
    global void finish(Database.BatchableContext bc){
    
    }    
}
