global class Weather {

    @AuraEnabled public String cityName;
    @AuraEnabled public Decimal cityTemp;
    @AuraEnabled public Decimal cityWindSpeed;
    @AuraEnabled public Decimal cityLat;
    @AuraEnabled public Decimal cityLong;
    @AuraEnabled public String description;
    @AuraEnabled public String icon;
}
